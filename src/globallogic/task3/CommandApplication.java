package globallogic.task3;

import globallogic.task3.command.OrderPlacer;
import globallogic.task3.model.OrderSide;
import globallogic.task3.service.MakeTradeSubscription;
import globallogic.task3.service.OrderListener;
import globallogic.task3.service.OrderService;

public class CommandApplication {

    public static void main(String[] args) {
        final OrderListener<OrderSide> orderListener = new OrderListener<>();
        final OrderPlacer orderPlacer = new OrderPlacer();
        final OrderService orderService = new OrderService();
        final MakeTradeSubscription makeTradeSubscription = new MakeTradeSubscription(orderPlacer, orderService);

        orderListener.addSubscription(makeTradeSubscription);
    }
}
