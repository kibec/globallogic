package globallogic.task3.command;

public class OrderPlacer {

    public void placeOrder(Order order) {
        order.execute();
    }
}
