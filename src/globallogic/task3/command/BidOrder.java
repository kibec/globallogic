package globallogic.task3.command;

import globallogic.task3.service.OrderService;

public class BidOrder implements Order {

    private final OrderService orderService;

    public BidOrder(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public void execute() {
        orderService.makeBid();
    }
}
