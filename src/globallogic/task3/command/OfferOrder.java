package globallogic.task3.command;

import globallogic.task3.service.OrderService;

public class OfferOrder implements Order {
    private final OrderService orderService;

    public OfferOrder(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public void execute() {
        orderService.makeOffer();
    }
}
