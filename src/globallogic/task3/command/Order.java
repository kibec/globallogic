package globallogic.task3.command;

public interface Order {
    void execute();
}
