package globallogic.task3.service;

import globallogic.task3.command.BidOrder;
import globallogic.task3.command.OfferOrder;
import globallogic.task3.command.OrderPlacer;
import globallogic.task3.model.OrderSide;

import java.util.function.Consumer;

public class MakeTradeSubscription implements Consumer<OrderSide> {

    private final OrderPlacer orderPlacer;
    private final OrderService orderService;

    public MakeTradeSubscription(OrderPlacer orderPlacer, OrderService orderService) {
        this.orderPlacer = orderPlacer;
        this.orderService = orderService;
    }

    @Override
    public void accept(OrderSide side) {
        switch (side) {
            case BID:
                orderPlacer.placeOrder(new OfferOrder(orderService));
                break;
            case OFFER:
                orderPlacer.placeOrder(new BidOrder(orderService));
                break;
        }

    }
}
