package globallogic.task3.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class OrderListener<T> {

    private final List<Consumer<T>> subscribers = new ArrayList<>();

    public void addSubscription(Consumer<T> subscription) {
        subscribers.add(subscription);
    }

    public void onMessage(T message) {
        subscribers.forEach(s -> s.accept(message));
    }

}
