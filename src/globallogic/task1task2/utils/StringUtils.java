package globallogic.task1task2.utils;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StringUtils {


    public static Character findFirstNotRepeatableCharV1(String input) {
        char[] chars = input.toCharArray();
        Boolean[] repetable = new Boolean[Character.MAX_VALUE];
        Arrays.fill(repetable, null);
        ArrayDeque<Character> nonRepeatable = new ArrayDeque<>();

        for (char aChar : chars) {
            aChar = Character.toLowerCase(aChar);
            Boolean repeated = repetable[aChar];
            if (repeated == null) {
                repetable[aChar] = true;
                nonRepeatable.add(aChar);
            } else if (repeated) {
                repetable[aChar] = false;
                nonRepeatable.remove(aChar);
            }
        }

        return nonRepeatable.poll();
    }

    public static Character findFirstNotRepeatableCharV2(String input) {
        return input.chars()
                .mapToObj(c -> Character.toLowerCase((char) c))
                .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .findFirst().get().getKey();
    }

}
