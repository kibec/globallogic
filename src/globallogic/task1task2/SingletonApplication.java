package globallogic.task1task2;

import globallogic.task1task2.transport.ListenerSingleton;
import globallogic.task1task2.utils.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SingletonApplication {


    public static void main(String[] args) throws IOException {

        Stream<String> lines = Files.lines(Paths.get("C:/voyna-i-mir-tom-3-fb2-txt.txt"));
        String input = lines.collect(Collectors.joining());

        ListenerSingleton<String> instance = ListenerSingleton.<String>getInstance();

        instance.addSubscription((s) -> StringUtils.findFirstNotRepeatableCharV1(s));
        instance.addSubscription((s) -> StringUtils.findFirstNotRepeatableCharV2(s));

        instance.onMessage(input);
    }

}
