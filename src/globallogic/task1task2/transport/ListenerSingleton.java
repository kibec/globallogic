package globallogic.task1task2.transport;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ListenerSingleton<T> {

    private static volatile ListenerSingleton instance = null;
    private final List<Consumer<T>> subscribers = new ArrayList<>();

    protected ListenerSingleton() {
    }

    public static <T> ListenerSingleton<T> getInstance() {
        if (instance == null) {
            synchronized (ListenerSingleton.class) {
                if (instance == null) {
                    instance = new ListenerSingleton<T>();
                }
            }
        }

        return instance;
    }

        public void addSubscription(Consumer<T> subscription) {
            subscribers.add(subscription);
        }

        public void onMessage(T message) {
            subscribers.forEach(s -> s.accept(message));
        }
}
