package globallogic.task1task2.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StringUtilsTest {

    @Test
    public void testBasicScenario() {
        String input = "Wawel Royal Castle";
        Character firstNotRepeatableChar = StringUtils.findFirstNotRepeatableCharV1(input);

        assertEquals('r', firstNotRepeatableChar.charValue());
        ;
    }

    @Test
    public void testWithoutNonRepeatable() {
        String input = "aaaaaaa";
        Character firstNotRepeatableChar = StringUtils.findFirstNotRepeatableCharV1(input);

        assertTrue(firstNotRepeatableChar == null);
    }

    @Test
    public void testUpperCaseShoudNotPassAsNonRepetable() {
        String input = "aaaAaaa";
        Character firstNotRepeatableChar = StringUtils.findFirstNotRepeatableCharV1(input);

        assertTrue(firstNotRepeatableChar == null);
    }

    @Test
    public void testEmptyString() {
        String input = "";
        Character firstNotRepeatableChar = StringUtils.findFirstNotRepeatableCharV1(input);

        assertTrue(firstNotRepeatableChar == null);
    }

}